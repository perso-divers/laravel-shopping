data "archive_file" "code" {
  type        = "zip"
  source_dir  = var.source_dir
  output_path = "./${var.project}-${var.name}${var.branch_suffix}.zip"
  excludes    = var.exclude_dirs
}

resource "google_storage_bucket" "code" {
  # checkov:skip=CKV_GCP_62:Code bucket
  # checkov:skip=CKV_GCP_78:Code bucket
  name                        = "code-${var.name}-${var.project}${var.branch_suffix}"
  location                    = var.project_region
  storage_class               = "REGIONAL"
  force_destroy               = false
  public_access_prevention    = "enforced"
  uniform_bucket_level_access = true
}

resource "google_storage_bucket_object" "code" {
  name   = "${var.project}-code.zip"
  bucket = google_storage_bucket.code.name
  source = data.archive_file.code.output_path
}
