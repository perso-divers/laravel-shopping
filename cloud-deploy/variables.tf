variable "project" {
  type        = string
  description = "Project destination to deploy de app"
  default     = "laravel-shopping-396710"
}

variable "name" {
  description = "Service name. Used to name the cloud run and all the ressources linked to it"
  type        = string
  default     = "laravel-shopping"
}

variable "branch_suffix" {
  type        = string
  description = "Branch being deployed"
  default     = ""
}

variable "impersonate_service_account" {
  type        = string
  description = "Account to impersonate to deploy"
  default     = "service-account-terraform@laravel-shopping-396710.iam.gserviceaccount.com"
}

variable "project_region" {
  type        = string
  description = "Project region"
  default     = "europe-west1"
}

variable "source_dir" {
  description = "Code directory relative path"
  type        = string
  default     = "../"
}

variable "exclude_dirs" {
  description = "Exclude directories"
  type        = list(string)
  default     = ["code/vendor", ".idea", ".git", "ci-cd"]
}

variable "dockerfile_path" {
  description = "Dockerfile path relative to source_dir value"
  type        = string
  default     = "./code/Dockerfile"
}

variable "secret_env_vars" {
  description = "Previously registered secrets available in the cloud run (stored in the secret manager) <pre>[\"NPM_REGISTRY_TOKEN\"]</pre>"
  type        = list(string)
  default     = []
}
