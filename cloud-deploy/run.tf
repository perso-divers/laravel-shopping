resource "null_resource" "cloud-run-api-image" {
  depends_on   = [google_artifact_registry_repository.artifact_repository]
  triggers = {
    on = google_storage_bucket_object.code.md5hash
  }

  provisioner "local-exec" {
    command = <<EOF
${path.module}/formatCloudBuildConfig.sh ${join(" ", var.secret_env_vars)}
gcloud builds submit '${google_storage_bucket.code.url}/${google_storage_bucket_object.code.name}' \
--config ${path.module}/cloudbuild.json \
--substitutions TAG_NAME=${data.archive_file.code.output_md5}${var.branch_suffix},_NAME=${var.name},_BRANCH_SUFFIX=${var.branch_suffix},_DOCKERFILE_PATH=${var.dockerfile_path} \
--impersonate-service-account=${var.impersonate_service_account} \
--region=europe-west1
EOF
  }
}

resource "google_cloud_run_v2_service" "api" {
  depends_on   = [null_resource.cloud-run-api-image]
  name         = "app"
  location     = var.project_region
  project      = var.project
  launch_stage = "BETA"
  ingress      = "INGRESS_TRAFFIC_ALL"

  template {
    containers {
      image = local.image
      ports {
        container_port = 80
      }
    }
    vpc_access{
      connector = google_vpc_access_connector.connector.id
      egress = "ALL_TRAFFIC"
    }
  }
#  traffic {
#    type = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
#    percent = 100
#  }
}
