terraform {
  required_version = "= 1.5.7"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "= 4.83.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "= 4.83.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "= 2.4.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "= 3.2.1"
    }
  }
}



