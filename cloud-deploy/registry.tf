resource "google_artifact_registry_repository" "artifact_repository" {
  #checkov:skip=CKV_GCP_81:Code artifact
  #checkov:skip=CKV_GCP_84:Code artifact
  count    = 1
  project  = var.project
  provider = google-beta

  location      = var.project_region
  format        = "DOCKER"
  repository_id = "${var.name}${var.branch_suffix}"
}
