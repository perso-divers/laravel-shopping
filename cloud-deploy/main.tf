provider "google" {
  project                     = var.project
  region                      = var.project_region
}

provider "google-beta" {
  project                     = var.project
  region                      = var.project_region
}

terraform {
  backend "gcs" {
    prefix = "terraform/state"
  }
}
