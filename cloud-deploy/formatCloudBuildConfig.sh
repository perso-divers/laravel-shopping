#!/usr/bin/env bash

scriptpath=`dirname $(realpath $0)`

baseFilePath=$scriptpath/cloudbuild-base.json
cloudbuildFilePath=$scriptpath/cloudbuild.json

cp "$baseFilePath" "$cloudbuildFilePath"

i=1;
for secret_env in "$@"
do
    cloudbuild="$(jq '.steps[1].args[1] += " --build-arg '"$secret_env"'" | .steps[1].secretEnv += ["'"$secret_env"'"] | .availableSecrets.secretManager += [{"versionName": "projects/$PROJECT_ID/secrets/'"$secret_env"'/versions/latest" , "env": "'"$secret_env"'"}]' "$cloudbuildFilePath")"
    echo "$cloudbuild" > "$cloudbuildFilePath"
    i=$((i + 1));
done

cloudbuild="$(jq '.steps[1].args[1] += " ."' "$cloudbuildFilePath")"
echo "$cloudbuild" > "$cloudbuildFilePath"

cat "$cloudbuildFilePath"

