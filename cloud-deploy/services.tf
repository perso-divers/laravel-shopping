resource "google_project_service" "cloudRun" {
  service                    = "run.googleapis.com"
  disable_dependent_services = false
}

resource "google_project_service" "compute" {
  service                    = "compute.googleapis.com"
  disable_dependent_services = false
}

resource "google_project_service" "cloudResourceManager" {
  service                    = "cloudresourcemanager.googleapis.com"
  disable_on_destroy         = false
  disable_dependent_services = false
}

resource "google_project_service" "vpcaccess" {
  service                    = "vpcaccess.googleapis.com"
  disable_dependent_services = false
}
