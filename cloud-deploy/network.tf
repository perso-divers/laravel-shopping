resource "google_compute_network" "myVPC" {
  depends_on     = [google_project_service.compute]
  project                 = var.project
  name                    = "run-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnetwork" {
  depends_on     = [google_project_service.compute]
  name          = "run-subnetwork"
  ip_cidr_range = "10.2.0.0/28"
  region        = var.project_region
  network       = google_compute_network.myVPC.id
}

resource "google_vpc_access_connector" "connector" {
  depends_on     = [google_project_service.vpcaccess]
  project       = var.project
  name          = "run-vpc"
  region        = var.project_region
  subnet {
    name = google_compute_subnetwork.subnetwork.name
  }
}
