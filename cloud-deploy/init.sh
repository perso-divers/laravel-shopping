export GOOGLE_IMPERSONATE_SERVICE_ACCOUNT="service-account-terraform@laravel-shopping-396710.iam.gserviceaccount.com"
echo "setting gcloud impersonate account to $GOOGLE_IMPERSONATE_SERVICE_ACCOUNT"

if [ "$1" = "gitlab" ]
then
    branch_slug=$CI_COMMIT_REF_SLUG
else
    branch_slug="$(git symbolic-ref --short HEAD | sed -e "s/\//-/g"| sed -e "s/\./-/g" | sed -e 's/\(.*\)/\L\1/')"
fi

project="laravel-shopping-396710"
echo "setting gcloud project to $project"
gcloud config set project $project

terraform init -backend-config="bucket=tfstate-laravel-shopping"

#if ! terraform workspace select "$branch_slug"; then
#    terraform workspace new "$branch_slug"
#fi
