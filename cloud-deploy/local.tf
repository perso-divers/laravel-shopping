locals {
  image = "europe-west1-docker.pkg.dev/${var.project}/${var.name}${var.branch_suffix}/${var.name}:${data.archive_file.code.output_md5}${var.branch_suffix}"
}
