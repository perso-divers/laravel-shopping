<?php

function formatPrice(float $price): string
{
    switch (getCurrency()) {
        case 'EUR':
            return number_format($price / 100, 2) . ' €';
        default:
            return '$ '. number_format($price / 100, 2);
    }
}

function getCurrency(): string
{
    return session('currency', 'USD');
}
