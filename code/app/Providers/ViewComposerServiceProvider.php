<?php

namespace App\Providers;

use App\Models\eCommerce\Cart;
use App\Models\User\User;
use App\Services\Repositories\Contracts\Product\ProductCategoryRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Illuminate\Support\Facades;

final class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Facades\View::composer('layout', function (View $view) {
            $user = auth()->user();
            if($user) {
                $userCart = $user->getCurrentCart();
            } else {
                $userCart = new Cart();
            }
            $view->with('currentCart', $userCart);
            $view->with('cartLines', $userCart->lines);
            $view->with('cartTotal', $userCart->getTotal());
            $view->with('categories', app(ProductCategoryRepositoryInterface::class)->all());
        });
    }
}
