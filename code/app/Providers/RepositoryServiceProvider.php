<?php

namespace App\Providers;

use App\Services\Repositories\Contracts\eCommerce\AddressRepositoryInterface;
use App\Services\Repositories\Contracts\eCommerce\CartRepositoryInterface;
use App\Services\Repositories\Contracts\eCommerce\OrderLineRepositoryInterface;
use App\Services\Repositories\Contracts\eCommerce\OrderRepositoryInterface;
use App\Services\Repositories\Contracts\eCommerce\PaymentProviderRepositoryInterface;
use App\Services\Repositories\Contracts\Product\ProductCategoryRepositoryInterface;
use App\Services\Repositories\Contracts\Product\ProductRepositoryInterface;
use App\Services\Repositories\Contracts\User\UserRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\eCommerce\AddressEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\eCommerce\CartEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\eCommerce\OrderEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\eCommerce\OrderLineEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\eCommerce\PaymentProviderEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\Product\ProductCategoryEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\Product\ProductEloquentRepository;
use App\Services\Repositories\Implementations\Eloquent\User\UserEloquentRepository;
use Illuminate\Support\ServiceProvider;

final class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(CartRepositoryInterface::class, CartEloquentRepository::class);
        $this->app->bind(ProductRepositoryInterface::class, ProductEloquentRepository::class);
        $this->app->bind(ProductCategoryRepositoryInterface::class, ProductCategoryEloquentRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderEloquentRepository::class);
        $this->app->bind(OrderLineRepositoryInterface::class, OrderLineEloquentRepository::class);
        $this->app->bind(PaymentProviderRepositoryInterface::class, PaymentProviderEloquentRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserEloquentRepository::class);
        $this->app->bind(AddressRepositoryInterface::class, AddressEloquentRepository::class);
    }
}
