<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\AbstractController;
use App\Services\Repositories\Contracts\Product\ProductRepositoryInterface;
use Illuminate\View\View;

final class ProductController extends AbstractController
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function show($id): View
    {
        return view('product.product.show', ['product' => $this->productRepository->findById($id)]);
    }

}
