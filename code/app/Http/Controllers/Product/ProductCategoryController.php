<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\AbstractController;
use App\Services\Repositories\Contracts\Product\ProductCategoryRepositoryInterface;
use Illuminate\View\View;

final class ProductCategoryController extends AbstractController
{
    private ProductCategoryRepositoryInterface $productCategoryRepository;

    public function __construct(ProductCategoryRepositoryInterface $productCategoryRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function show($id): View
    {
        return view('product.category.show', ['category' => $this->productCategoryRepository->findById($id)]);
    }

}
