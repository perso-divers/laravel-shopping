<?php

namespace App\Http\Controllers\eCommerce;

use App\Http\Controllers\AbstractController;
use App\Models\eCommerce\Address;
use App\Models\eCommerce\Cart;
use App\Models\eCommerce\PaymentProvider;
use App\Services\eCommerce\Cart\AddProductToCart;
use App\Services\Repositories\Contracts\eCommerce\AddressRepositoryInterface;
use App\Services\Repositories\Contracts\eCommerce\PaymentProviderRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\View\View;

final class CartController extends AbstractController
{
    private PaymentProviderRepositoryInterface $paymentProviderRepository;
    private AddressRepositoryInterface $addressRepository;

    public function __construct(
        PaymentProviderRepositoryInterface $paymentProviderRepository,
        AddressRepositoryInterface $addressRepository
    ) {
        $this->paymentProviderRepository = $paymentProviderRepository;
        $this->addressRepository = $addressRepository;
    }

    public function show(): View
    {
        $user = auth()->user();
        $userCart = $user->getCurrentCart();

        return view('eCommerce.cart.cart', [
            'cart' => $userCart,
            'paymentProviders' => $this->paymentProviderRepository->all(),
            'total' => $userCart->getTotal(),
        ]);
    }

    public function addToCard($product_id): RedirectResponse
    {
        $user = auth()->user();
        $cart = $user->getCurrentCart();
        (new AddProductToCart())->add($cart, $product_id, 1);

        return redirect()->back();
    }

    public function validateCart($provider_name_slug): RedirectResponse
    {
        $provider = PaymentProvider::all()->filter(function ($provider) use ($provider_name_slug) {
            return Str::slug($provider->name)  === $provider_name_slug;
        })->first();

        abort_if(!$provider, 404);

        $cart = auth()->user()->getCurrentCart();
        $urlToPay = app($provider->class)->getUrlToPay($cart);

        $cart->payment_provider_id = $provider->id;
        $cart->save();

        return redirect($urlToPay);
    }

    public function thanks(): View
    {
        return view('eCommerce.cart.thanks');
    }

    public function successOrder(Request $request)
    {
        $provider = $this->getProviderFromRequest($request->all());
        app($provider->class)->successOrder(@file_get_contents('php://input'), $request->header());
    }

    private function getProviderFromRequest(array $data)
    {
        if (str_contains($data['id'], 'evt_')) {
            return PaymentProvider::where('name', 'stripe')->first();
        } else {
            return PaymentProvider::where('name', 'paypal')->first();
        }
    }

    public function saveAddresses(Request $request)
    {
        $user = auth()->user();
        $cart = $user->getCurrentCart();
        $data = $request->all();
        $this->addressRepository->updateOrCreate([
            'type' => Address::TYPE_SHIPPING,
            'addressable_type' => Cart::class,
            'addressable_id' => $cart->id
        ], $data['shipping']);
        $this->addressRepository->updateOrCreate([
            'type' => Address::TYPE_DELIVERY,
            'addressable_type' => Cart::class,
            'addressable_id' => $cart->id
        ], $data['delivery']);

        return redirect()->back();
    }

}
