<?php

namespace App\Http\Controllers\eCommerce;

use App\Http\Controllers\AbstractController;
use App\Models\eCommerce\Order;
use App\Services\Repositories\Contracts\eCommerce\OrderRepositoryInterface;
use Illuminate\View\View;

final class OrderController extends AbstractController
{
    private OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function show($id): View
    {
        /** @var Order $order */
        $order = $this->orderRepository->findById($id);

        return view('eCommerce.order.show', [
            'order' => $this->orderRepository->findById($id),
            'total' => $order->getTotal(),
        ]);
    }

}
