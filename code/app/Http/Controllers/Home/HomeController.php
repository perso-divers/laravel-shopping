<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\AbstractController;
use App\Services\Repositories\Contracts\Product\ProductCategoryRepositoryInterface;
use Illuminate\View\View;

final class HomeController extends AbstractController
{
    private ProductCategoryRepositoryInterface $productCategoryRepository;

    public function __construct(
        ProductCategoryRepositoryInterface $productCategoryRepository
    ) {
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function index(): View
    {
        return view('home.index', [
            'categories' => $this->productCategoryRepository->all()
        ]);
    }

}
