<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\AbstractController;
use App\Models\User\User;
use App\Services\Repositories\Contracts\User\UserRepositoryInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

final class AuthController extends AbstractController
{
    private UserRepositoryInterface $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(): View
    {
        return view('auth.login');
    }

    public function doLogin(Request $request): RedirectResponse
    {
        $isLogin = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ]);

        if($isLogin) {
            $request->session()->regenerate();

            return redirect()->route('home');
        }

        return redirect()->back();
    }

    public function createUser(Request $request): RedirectResponse
    {
        /** @var User $user */
        $user = $this->userRepository->create($request->all());
        Auth::login($user);

        return redirect()->route('home');
    }

    public function logout(): RedirectResponse
    {
        Auth::logout();

        return redirect()->route('home');
    }

}
