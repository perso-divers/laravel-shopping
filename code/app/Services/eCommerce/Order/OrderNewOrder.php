<?php

namespace App\Services\eCommerce\Order;

use App\Models\eCommerce\Cart;
use App\Models\eCommerce\Order;
use App\Models\eCommerce\OrderLine;

final class OrderNewOrder
{
    private Cart $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function create(): Order
    {
        $order = new Order();
        $order->user_id = $this->cart->user_id;
        $order->save();

        foreach ($this->cart->lines as $line) {
            $orderLine = new OrderLine();
            $orderLine->order_id = $order->id;
            $orderLine->quantity = $line->quantity;
            $orderLine->product_id = $line->product_id;
            $orderLine->save();
        }

        $this->cart->delete();

        return $order;
    }

}
