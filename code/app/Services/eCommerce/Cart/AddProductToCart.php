<?php

namespace App\Services\eCommerce\Cart;

use App\Models\eCommerce\Cart;
use App\Models\eCommerce\CartLine;

final class AddProductToCart
{
    public function add(Cart $userCart, int $product_id, int $quantity): void
    {
        $newCartLine = new CartLine();
        $newCartLine->cart_id = $userCart->id;
        $newCartLine->quantity = $quantity;
        $newCartLine->product_id = $product_id;
        $newCartLine->save();
    }

}
