<?php

namespace App\Services\eCommerce\PaymentProviders\Implementations;

use App\Services\eCommerce\PaymentProviders\PaymentProviderInterface;
use GuzzleHttp\Client;
use App\Models\eCommerce\Cart;
use Illuminate\Support\Facades\Log;

final class PaypalProvider extends AbstractProvider implements PaymentProviderInterface
{
    public function getUrlToPay(Cart $cart): string
    {
        $client_id = config('ecommerce.paypal.client');
        $secret = config('ecommerce.paypal.secret');
        $client = new Client();

        try {
            $response = $client->request('POST', 'https://api-m.sandbox.paypal.com/v2/checkout/orders', [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => "Basic " . base64_encode("$client_id:$secret")
                ],
                'json' => [
                    "intent" => "CAPTURE",
                    "purchase_units" => [
                        [
                            "amount" => [
                                "currency_code" => "EUR",
                                "value" => $cart->getTotal()
                            ]
                        ]
                    ],
                    "payment_source" => [
                        "paypal" => [
                            "experience_context" => [
                                "user_action" => "PAY_NOW",
                                "return_url" => $this->getSuccessUrl(),
                                "cancel_url" => $this->getCancelUrl()
                            ]
                        ]
                    ]
                ],
            ]);

            $content = json_decode($response->getBody()->getContents(), true);
            $links = collect($content ['links']);
            $goodLink = $links->filter(function ($link) {
                return $link['rel'] === 'payer-action';
            })->first()['href'];

            $cart->payment_reference = $content['id'];
            $cart->save();

        } catch (\Exception $exception) {
            dd($exception);
        }

        return $goodLink;
    }

    //TODO
    public function successOrder(string $body, array $headers): void
    {
        Log::info('call Paypal!');
        Log::info($body);
    }
}
