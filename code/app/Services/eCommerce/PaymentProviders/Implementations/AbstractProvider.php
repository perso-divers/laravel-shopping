<?php

namespace App\Services\eCommerce\PaymentProviders\Implementations;

use App\Models\eCommerce\Cart;
use App\Services\eCommerce\Order\OrderNewOrder;
use App\Services\Repositories\Contracts\eCommerce\CartRepositoryInterface;

abstract class AbstractProvider
{

    private CartRepositoryInterface $cartRepository;

    public function __construct(CartRepositoryInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    protected function getSuccessUrl(): string
    {
        return route('cart.thanks');
    }

    protected function notifSuccess(): string
    {
        return route('cart.successOrder');
    }

    protected function getCancelUrl(): string
    {
        return route('cart.show');
    }

    protected function createOrder(string $payment_reference): void
    {
        /** @var Cart $userCart */
        $userCart = $this->cartRepository->findCartByReferenceProvider($payment_reference);
        (new OrderNewOrder($userCart))->create();
    }

}
