<?php

namespace App\Services\eCommerce\PaymentProviders\Implementations;

use App\Models\eCommerce\Cart;
use App\Services\eCommerce\PaymentProviders\PaymentProviderInterface;
use Illuminate\Support\Facades\Log;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Stripe\Webhook;

final class StripeProvider extends AbstractProvider implements PaymentProviderInterface
{
    public function getUrlToPay(Cart $cart): string
    {
        Stripe::setApiKey(config('ecommerce.stripe.secret'));
        Stripe::setApiVersion(config('ecommerce.stripe.api_version'));

        $line_items = $cart->lines->map( function ($line) {
            return [
                'quantity' => $line->quantity,
                'price_data' => [
                    'currency' => getCurrency(),
                    'product_data' => [
                        'name' => $line->product->name
                    ],
                    'unit_amount' => $line->product->price
                ]
            ];
        });

        $checkout_session = Session::create([
            'mode' => 'payment',
            'line_items' => $line_items->toArray(),
            'success_url' => $this->getSuccessUrl(),
            'cancel_url' => $this->getCancelUrl(),
            'payment_method_types' => ['card'],
        ]);

        $cart->payment_reference = $checkout_session->id;
        $cart->save();

        return $checkout_session->url;
    }

    public function successOrder(string $body, array $headers): void
    {
        $event = Webhook::constructEvent($body, $headers['stripe-signature'][0], config('ecommerce.stripe.cli_secret'));

        switch ($event->type) {
            case 'checkout.session.completed':
                try {
                    $paymentIntent = $event->data->object;
                    $this->createOrder($paymentIntent->id);
                } catch (\Exception $exception) {
                    Log::error('Error event checkout.session.completed', ['error' => $exception]);
                }
            default:
                Log::info('Event received but not managed', ['type'=> $event->type]);
        }
    }
}
