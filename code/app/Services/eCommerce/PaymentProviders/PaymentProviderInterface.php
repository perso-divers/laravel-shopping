<?php

namespace App\Services\eCommerce\PaymentProviders;

use App\Models\eCommerce\Cart;

interface PaymentProviderInterface
{
    public function getUrlToPay(Cart $cart): string;

    public function successOrder(string $body, array $headers): void;

}
