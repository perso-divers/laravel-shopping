<?php

namespace App\Services\Repositories\Implementations\Eloquent\User;

use App\Models\User\User;
use App\Services\Repositories\Contracts\User\UserRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class UserEloquentRepository extends EloquentRepository implements UserRepositoryInterface
{
    protected function getModel(): string
    {
        return User::class;
    }
}
