<?php

namespace App\Services\Repositories\Implementations\Eloquent\Product;

use App\Models\Product\ProductCategory;
use App\Services\Repositories\Contracts\Product\ProductCategoryRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class ProductCategoryEloquentRepository extends EloquentRepository implements ProductCategoryRepositoryInterface
{
    protected function getModel(): string
    {
        return ProductCategory::class;
    }
}
