<?php

namespace App\Services\Repositories\Implementations\Eloquent\Product;

use App\Models\Product\Product;
use App\Services\Repositories\Contracts\Product\ProductRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class ProductEloquentRepository extends EloquentRepository implements ProductRepositoryInterface
{
    public function getModel(): string
    {
        return Product::class;
    }

}
