<?php

namespace App\Services\Repositories\Implementations\Eloquent;

use App\Services\Repositories\Contracts\AbstractRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class EloquentRepository implements AbstractRepositoryInterface
{
    abstract protected function getModel(): string;

    protected function newModel(): Model
    {
        return new ($this->getModel());
    }

    public function all(): Collection
    {
        return $this->newModel()::all();
    }

    public function findById(string $id): Model
    {
        return $this->newModel()::find($id);
    }

    public function create(array $data): Model
    {
        return $this->newModel()::create($data);
    }

    public function updateOrCreate(array $search, array $data): Model
    {
        return $this->newModel()::updateOrCreate($search, $data);
    }

}
