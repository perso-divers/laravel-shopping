<?php

namespace App\Services\Repositories\Implementations\Eloquent\eCommerce;

use App\Models\eCommerce\Cart;
use Illuminate\Database\Eloquent\Model;
use App\Services\Repositories\Contracts\eCommerce\CartRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class CartEloquentRepository extends EloquentRepository implements CartRepositoryInterface
{
    protected function getModel(): string
    {
        return Cart::class;
    }

    public function findCartByReferenceProvider(string $referenceProvider): Model | null
    {
        return $this->newModel()->where("payment_reference", $referenceProvider)->first();
    }

}
