<?php

namespace App\Services\Repositories\Implementations\Eloquent\eCommerce;

use App\Models\eCommerce\Address;
use App\Services\Repositories\Contracts\eCommerce\AddressRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class AddressEloquentRepository extends EloquentRepository implements AddressRepositoryInterface
{
    protected function getModel(): string
    {
        return Address::class;
    }
}
