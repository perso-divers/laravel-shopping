<?php

namespace App\Services\Repositories\Implementations\Eloquent\eCommerce;

use App\Models\eCommerce\CartLine;
use App\Services\Repositories\Contracts\eCommerce\OrderLineRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class OrderLineEloquentRepository extends EloquentRepository implements OrderLineRepositoryInterface
{
    protected function getModel(): string
    {
        return CartLine::class;
    }
}
