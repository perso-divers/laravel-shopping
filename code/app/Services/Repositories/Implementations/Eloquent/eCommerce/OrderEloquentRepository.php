<?php

namespace App\Services\Repositories\Implementations\Eloquent\eCommerce;

use App\Models\eCommerce\Order;
use App\Services\Repositories\Contracts\eCommerce\OrderRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class OrderEloquentRepository extends EloquentRepository implements OrderRepositoryInterface
{
    public function getModel(): string
    {
        return Order::class;
    }

}
