<?php

namespace App\Services\Repositories\Implementations\Eloquent\eCommerce;

use App\Models\eCommerce\PaymentProvider;
use App\Services\Repositories\Contracts\eCommerce\PaymentProviderRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class PaymentProviderEloquentRepository extends EloquentRepository implements PaymentProviderRepositoryInterface
{
    protected function getModel(): string
    {
        return PaymentProvider::class;
    }

}
