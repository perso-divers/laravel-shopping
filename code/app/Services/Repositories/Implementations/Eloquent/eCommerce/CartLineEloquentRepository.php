<?php

namespace App\Services\Repositories\Implementations\Eloquent\eCommerce;

use App\Models\eCommerce\CartLine;
use App\Services\Repositories\Contracts\eCommerce\CartLineRepositoryInterface;
use App\Services\Repositories\Implementations\Eloquent\EloquentRepository;

final class CartLineEloquentRepository extends EloquentRepository implements CartLineRepositoryInterface
{
    protected function getModel(): string
    {
        return CartLine::class;
    }
}
