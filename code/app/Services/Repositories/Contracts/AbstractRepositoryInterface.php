<?php

namespace App\Services\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface AbstractRepositoryInterface
{
    public function all(): Collection;

    public function findById(string $id): Model;

    public function create(array $data): Model;

    public function updateOrCreate(array $search, array $data): Model;

}
