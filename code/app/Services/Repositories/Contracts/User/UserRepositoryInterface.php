<?php

namespace App\Services\Repositories\Contracts\User;

use App\Services\Repositories\Contracts\AbstractRepositoryInterface;

interface UserRepositoryInterface extends AbstractRepositoryInterface
{
}
