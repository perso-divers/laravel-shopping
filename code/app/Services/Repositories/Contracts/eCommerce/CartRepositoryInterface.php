<?php

namespace App\Services\Repositories\Contracts\eCommerce;

use Illuminate\Database\Eloquent\Model;
use App\Services\Repositories\Contracts\AbstractRepositoryInterface;

interface CartRepositoryInterface extends AbstractRepositoryInterface
{

    public function findCartByReferenceProvider(string $referenceProvider): Model | null;

}
