<?php

namespace App\Services\Repositories\Contracts\eCommerce;

use App\Services\Repositories\Contracts\AbstractRepositoryInterface;

interface CartLineRepositoryInterface extends AbstractRepositoryInterface
{
}
