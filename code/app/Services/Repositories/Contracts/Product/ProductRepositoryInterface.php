<?php

namespace App\Services\Repositories\Contracts\Product;

use App\Services\Repositories\Contracts\AbstractRepositoryInterface;

interface ProductRepositoryInterface extends AbstractRepositoryInterface
{
}
