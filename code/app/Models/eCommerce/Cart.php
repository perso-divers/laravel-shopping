<?php

namespace App\Models\eCommerce;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphOne;

final class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'payment_provider_id',
        'payment_reference'
    ];

    public function lines(): HasMany
    {
        return $this->hasMany(CartLine::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, CartLine::class);
    }

    public function provider(): BelongsTo
    {
        return $this->belongsTo(PaymentProvider::class);
    }

    public function count(): int
    {
        return $this->lines->count();
    }

    public function getTotal(): float
    {
        return $this->products()->pluck('price')->reduce(function (int $carry, int $productPrice) {
            return $carry + $productPrice;
        }, 0);
    }

    public function delivery_address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable')->where('type', Address::TYPE_DELIVERY);
    }

    public function shipping_address(): MorphOne
    {
        return $this->morphOne(Address::class, 'addressable')->where('type', Address::TYPE_SHIPPING);
    }

}
