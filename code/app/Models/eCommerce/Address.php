<?php

namespace App\Models\eCommerce;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

final class Address extends Model
{
    use HasFactory;

    const TYPE_SHIPPING = 10;
    const TYPE_DELIVERY = 20;

    protected $fillable = [
        'firstname',
        'lastname',
        'street',
        'zipcode',
        'city',
        'country',
        'addressable_type',
        'addressable_id',
        'type',
    ];

    public function addressable(): MorphTo
    {
        return $this->morphTo();
    }

}
