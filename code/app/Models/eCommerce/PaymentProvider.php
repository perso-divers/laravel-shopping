<?php

namespace App\Models\eCommerce;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;

final class PaymentProvider extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'class',
        'icon',
        'description'
    ];

    public $timestamps = false;

}
