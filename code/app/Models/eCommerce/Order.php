<?php

namespace App\Models\eCommerce;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

final class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id'
    ];

    public function lines(): HasMany
    {
        return $this->hasMany(OrderLine::class);
    }

    public function products(): HasManyThrough
    {
        return $this->hasManyThrough(Product::class, OrderLine::class, 'order_id', 'id');
    }

    public function getTotal(): float
    {
        return $this->products()->pluck('price')->reduce(function (?int $carry, int $productPrirce) {
            return $carry + $productPrirce;
        });
    }

}
