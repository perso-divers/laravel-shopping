<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'description_long',
        'price',
        'category_id',
    ];

    public function url(): string
    {
        return route('products.show', $this->id);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function comments(): HasMany
    {
        return $this->hasMany(ProductReview::class);
    }

    public function getAverageNote(): float
    {
        return $this->comments
            ->filter(function ($comment) {
                return $comment->note !== null;
            })
            ->pluck('note')
            ->average();
    }

}
