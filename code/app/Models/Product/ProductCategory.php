<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class ProductCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
    ];

    public function url(): string
    {
        return route('product_categories.show', $this->id);
    }

    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'category_id');
    }

}
