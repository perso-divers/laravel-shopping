<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\eCommerce\CartController;
use App\Http\Controllers\eCommerce\OrderController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Product\ProductCategoryController;
use App\Http\Controllers\Product\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::get('/my-cart', [CartController::class, 'show'])->name('cart.show');
    Route::get('/carts/add-product/{id}', [CartController::class, 'addToCard'])->name('cart.addToCard');
    Route::get('/carts/validate/{provider}', [CartController::class, 'validateCart'])->name('cart.validate');
    Route::get('/carts/thanks', [CartController::class, 'thanks'])->name('cart.thanks');
    Route::post('/carts/saveAddresses', [CartController::class, 'saveAddresses'])->name('cart.saveAddresses');

    Route::get('/orders/{id}', [OrderController::class, 'show'])->name('orders.show');

    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/products/{id}', [ProductController::class, 'show'])->name('products.show');
Route::get('/product-categories/{id}', [ProductCategoryController::class, 'show'])->name('product_categories.show');

Route::get('/login', [AuthController::class, 'login'])->name('auth.login');
Route::post('/login', [AuthController::class, 'doLogin'])->name('auth.doLogin');
Route::post('/create-account', [AuthController::class, 'createUser'])->name('auth.createAccount');

