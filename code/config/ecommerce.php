<?php

return [
    'paypal' => [
        'url' => 'https://api-m.sandbox.paypal.com/v2/checkout/orders',
        'client' => env('PAYPAL_CLIENT'),
        'secret' => env('PAYPAL_SECRET')
    ],
    'stripe' => [
        'api_version' => '2023-08-16',
        'cli_secret' => env('CLI_SECRET'),
        'client' => env('STRIPE_CLIENT'),
        'secret' => env('STRIPE_SECRET')
    ]
];
