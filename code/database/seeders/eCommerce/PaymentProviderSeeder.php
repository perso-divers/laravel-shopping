<?php

namespace Database\Seeders\eCommerce;

use App\Models\eCommerce\PaymentProvider;
use App\Services\eCommerce\PaymentProviders\Implementations\PaypalProvider;
use App\Services\eCommerce\PaymentProviders\Implementations\StripeProvider;
use Illuminate\Database\Seeder;

final class PaymentProviderSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        PaymentProvider::firstOrCreate([
            'name' => 'Paypal',
            'description' => "Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.",
            'icon' => "img/payment/1.png",
            'class' => PaypalProvider::class
        ], ['name' => 'Paypal']);
        PaymentProvider::firstOrCreate([
            'name' => 'Stripe',
            'description' => "Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won't be shipped until the funds have cleared in our account.",
            'icon' => "img/payment/3.png",
            'class' => StripeProvider::class
        ], ['name' => 'Stripe']);
    }
}
