<?php

namespace Database\Seeders;

use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductReview;
use App\Models\User\User;
use Database\Seeders\eCommerce\PaymentProviderSeeder;
use Illuminate\Database\Seeder;

final class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        app(PaymentProviderSeeder::class)->run();
        User::factory(10)->create();
        ProductCategory::factory(4)->create();
        Product::factory(30)->create();
        ProductReview::factory(300)->create();
    }
}
