<?php

namespace Database\Factories\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

final class ProductCategoryFactory extends Factory
{

   public function definition(): array
    {
        return [
            'name' => fake()->name(),
        ];
    }
}
