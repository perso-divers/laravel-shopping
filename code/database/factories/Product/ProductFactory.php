<?php

namespace Database\Factories\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

final class ProductFactory extends Factory
{

    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'description' => fake()->text(150),
            'description_long' => fake()->text(650),
            'price' => fake()->numberBetween(100, 45000),
            'category_id' => fake()->numberBetween(1, 4),
        ];
    }
}
