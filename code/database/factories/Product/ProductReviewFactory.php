<?php

namespace Database\Factories\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

final class ProductReviewFactory extends Factory
{

    public function definition(): array
    {
        return [
            'product_id' => fake()->numberBetween(1, 30),
            'user_id' => fake()->numberBetween(1, 10),
            'comment' => fake()->text(150),
            'note' => fake()->numberBetween(0, 5),
        ];
    }
}
