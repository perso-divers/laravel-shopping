@extends('layout')

@section('content')
    <div class="purchase-online-area pt-80">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="title-border">{{ $category->name }}</h2>
            </div>
            <div class="row">
                @foreach($category->products as $product)
                    @component('product.product.includes.item', ['product' => $product])@endcomponent
                @endforeach
            </div>
        </div>
    </div>
@endsection
