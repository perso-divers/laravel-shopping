@extends('layout')

@section('content')

<!-- HEADING-BANNER START -->
<div class="heading-banner-area overlay-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-banner">
                    <div class="heading-banner-title">
                        <h2>{{ $product->name }}</h2>
                    </div>
                    <div class="breadcumbs pb-15">
                        <ul>
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ $product->category->url() }}">{{ $product->category->name }}</a></li>
                            <li>{{ $product->name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- HEADING-BANNER END -->
<!-- PRODUCT-AREA START -->
<div class="product-area single-pro-area pt-80 pb-80 product-style-2">
    <div class="container">
        <div class="row shop-list single-pro-info no-sidebar">
            <!-- Single-product start -->
            <div class="col-lg-12">
                <div class="single-product clearfix">
                    <!-- Single-pro-slider Big-photo start -->
                    <div class="single-pro-slider single-big-photo view-lightbox slider-for">
                        <div>
                            <img src="/img/single-product/medium/1.jpg" alt="" />
                            <a class="view-full-screen" href="img/single-product/large/1.jpg"  data-lightbox="roadtrip" data-title="My caption">
                                <i class="zmdi zmdi-zoom-in"></i>
                            </a>
                        </div>
                        <div>
                            <img src="/img/single-product/medium/2.jpg" alt="" />
                            <a class="view-full-screen" href="img/single-product/large/2.jpg"  data-lightbox="roadtrip" data-title="My caption">
                                <i class="zmdi zmdi-zoom-in"></i>
                            </a>
                        </div>
                        <div>
                            <img src="/img/single-product/medium/3.jpg" alt="" />
                            <a class="view-full-screen" href="img/single-product/large/3.jpg"  data-lightbox="roadtrip" data-title="My caption">
                                <i class="zmdi zmdi-zoom-in"></i>
                            </a>
                        </div>
                        <div>
                            <img src="img/single-product/medium/4.jpg" alt="" />
                            <a class="view-full-screen" href="img/single-product/large/4.jpg"  data-lightbox="roadtrip" data-title="My caption">
                                <i class="zmdi zmdi-zoom-in"></i>
                            </a>
                        </div>
                        <div>
                            <img src="/img/single-product/medium/5.jpg" alt="" />
                            <a class="view-full-screen" href="img/single-product/large/5.jpg"  data-lightbox="roadtrip" data-title="My caption">
                                <i class="zmdi zmdi-zoom-in"></i>
                            </a>
                        </div>
                    </div>
                    <!-- Single-pro-slider Big-photo end -->
                    <div class="product-info">
                        <div class="fix">
                            <h4 class="post-title floatleft">{{ $product->name }}</h4>
                            <span class="pro-rating floatright">
                                @component('product.product.includes.stars', ['product' => $product])@endcomponent
                                <span>( {{$product->comments()->count()}} Rating )</span>
                            </span>
                        </div>
                        <div class="fix mb-20">
                            <span class="pro-price">{{ formatPrice($product->price) }}</span>
                        </div>
                        <div class="product-description">
                            <p>{{ $product->description }}</p>
                        </div>
                        <div class="clearfix">
                            <div class="cart-plus-minus">
                                <input type="text" value="1" name="qtybutton" class="cart-plus-minus-box">
                            </div>
                            <div class="product-action clearfix">
                                <a href="wishlist.html" data-bs-toggle="tooltip" data-placement="top" title="Wishlist"><i class="zmdi zmdi-favorite-outline"></i></a>
                                <a href="#" data-bs-toggle="modal"  data-bs-target="#productModal" title="Quick View"><i class="zmdi zmdi-zoom-in"></i></a>
                                <a href="{{ route('cart.addToCard', $product->id) }}" data-bs-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                            </div>
                        </div>
                        <!-- Single-pro-slider Small-photo start -->
                        <div class="single-pro-slider single-sml-photo slider-nav">
                            <div>
                                <img src="/img/single-product/small/1.jpg"/>
                            </div>
                            <div>
                                <img src="/img/single-product/small/2.jpg"/>
                            </div>
                            <div>
                                <img src="/img/single-product/small/3.jpg"/>
                            </div>
                            <div>
                                <img src="/img/single-product/small/4.jpg"/>
                            </div>
                            <div>
                                <img src="/img/single-product/small/5.jpg"/>
                            </div>
                        </div>
                        <!-- Single-pro-slider Small-photo end -->
                    </div>
                </div>
            </div>
            <!-- Single-product end -->
        </div>
        <!-- single-product-tab start -->
        <div class="single-pro-tab">
            <div class="row">
                <div class="col-md-3">
                    <div class="single-pro-tab-menu">
                        <!-- Nav tabs -->
                        <ul class="nav d-block">
                            <li><a href="#description" data-bs-toggle="tab">Description</a></li>
                            <li><a class="active" href="#reviews"  data-bs-toggle="tab">Reviews</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane" id="description">
                            <div class="pro-tab-info pro-description">
                                <h3 class="tab-title title-border mb-30">{{ $product->name }}</h3>
                                {{ $product->description_long }}
                            </div>
                        </div>
                        <div class="tab-pane active" id="reviews">
                            <div class="pro-tab-info pro-reviews">
                                <div class="customer-review mb-60">
                                    <h3 class="tab-title title-border mb-30">Customer review</h3>
                                    <ul class="product-comments clearfix">
                                        @foreach($product->comments as $review)
                                            <li class="mb-30">
                                                <div class="pro-reviewer">
                                                    <img src="/img/reviewer/1.jpg" alt="" />
                                                </div>
                                                <div class="pro-reviewer-comment">
                                                    <div class="fix">
                                                        <div class="floatleft mbl-center">
                                                            <h5 class="text-uppercase mb-0"><strong>{{ $review->user->name }}</strong></h5>
                                                            <p class="reply-date">{{ $review->created_at->format('d/m/Y') }}</p>
                                                        </div>
                                                    </div>
                                                    <p class="mb-0">{{ $review->comment }}</p>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- single-product-tab end -->
    </div>
</div>
<!-- PRODUCT-AREA END -->

@endsection
