<div class="single-product col-xl-3 col-lg-4 col-md-6">
    <div class="product-img">
        <a href="{{ $product->url() }}"><img src="/img/product/3.jpg"/></a>
        <div class="product-action clearfix">
            <a href="#" data-bs-toggle="modal" data-bs-target="#productModal" title="Favorite"><i class="zmdi zmdi-favorite-outline"></i></a>
            <a href="#" data-bs-toggle="modal" data-bs-target="#productModal{{$product->id}}" title="Quick View"><i class="zmdi zmdi-zoom-in"></i></a>
            <a href="{{ route('cart.addToCard', $product->id) }}" data-bs-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
        </div>
    </div>
    <div class="product-info clearfix">
        <div class="fix">
            <h4 class="post-title floatleft"><a href="#">{{ $product->name }}</a></h4>
            <p class="floatright hidden-sm">{{ $product->category->name }}</p>
        </div>
        <div class="fix">
            <span class="pro-price floatleft">{{ formatPrice($product->price) }}</span>
            <span class="pro-rating floatright">
              @component('product.product.includes.stars', ['product' => $product])@endcomponent
            </span>
        </div>
    </div>
</div>
