@php($note = $product->getAverageNote())

@for ($i = 1; $i <= 5; $i++)
    @if($note > 2/3)
        <a href="#"><i class="zmdi zmdi-star"></i></a>
    @elseif($note > 1/3)
        <a href="#"><i class="zmdi zmdi-star-half"></i></a>
    @else
        <a href="#"><i class="zmdi zmdi-star-outline"></i></a>
    @endif
    @php($note--)
@endfor
