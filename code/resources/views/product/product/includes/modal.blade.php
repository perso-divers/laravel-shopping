<div id="quickview-wrapper">
    <div class="modal fade" id="productModal{{$product->id}}" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="modal-product">
                        <div class="product-images">
                            <div class="main-image images">
                                <img alt="#" src="img/product/quickview-photo.jpg"/>
                            </div>
                        </div><!-- .product-images -->
                        <div class="product-info">
                            <h1>{{ $product->name }}</h1>
                            <div class="price-box-3">
                                <hr />
                                <div class="s-price-box">
                                    <span class="new-price">{{ formatPrice($product->price) }}</span>
                                </div>
                                <hr />
                            </div>
                            <div class="quick-add-to-cart">
                                <form method="post" class="cart">
                                    <div class="numbers-row">
                                        <input type="number" id="french-hens" value="1" min="1">
                                    </div>
                                    <button class="single_add_to_cart_button" type="submit">Add to cart</button>
                                </form>
                            </div>
                            <div class="quick-desc">{{ $product->description }}</div>
                        </div><!-- .product-info -->
                    </div><!-- .modal-product -->
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>
    <!-- END Modal -->
</div>
<!-- END QUICKVIEW PRODUCT -->
