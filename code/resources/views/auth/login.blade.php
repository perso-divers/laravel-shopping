@extends('layout')

@section('content')
    <div class="login-area pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="customer-login text-left">
                        <form action="{{ route('auth.doLogin') }}" method="post">
                            @csrf
                            <h4 class="title-1 title-border text-uppercase mb-30">Registered customers</h4>
                            <p class="text-gray">If you have an account with us, Please login!</p>
                            <input type="text" placeholder="Email here..." name="email" value="{{ old('email') }}">
                            @error('email') {{$message}} @enderror
                            <input type="password" name="password" placeholder="Password">
                            @error('password') {{$message}} @enderror
                            <button type="submit" data-text="login" class="button-one submit-button mt-15">login</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="customer-login text-left">
                        <form action="{{ route('auth.createAccount') }}" method="post">
                            @csrf
                            <h4 class="title-1 title-border text-uppercase mb-30">new customers</h4>
                            <p class="text-gray">If you have an account with us, Please login!</p>
                            <input type="text" placeholder="Your firstname here..." name="firstname">
                            <input type="text" placeholder="Your lastname here..." name="lastname">
                            <input type="email" placeholder="Email address here..." name="email">
                            <input type="password" name="password" placeholder="Password">
                            <input type="password" name="password_confirm" placeholder="Confirm password">
                            <button type="submit" data-text="regiter" class="button-one submit-button mt-15">regiter</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
