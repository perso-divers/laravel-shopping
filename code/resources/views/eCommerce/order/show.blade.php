@extends('layout')

@section('content')
    <div class="shopping-cart-area  pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping-cart">
                        <div class="tab-content">
                            <div class="tab-pane active" id="order-complete">
                                <form action="#">
                                    <div class="thank-recieve bg-white mb-30">
                                        <p>Thank you. Your order has been received.</p>
                                    </div>
                                    <div class="order-info bg-white text-center clearfix mb-30">
                                        <div class="single-order-info">
                                            <h4 class="title-1 text-uppercase text-light-black mb-0">order n°</h4>
                                            <p class="text-uppercase text-light-black mb-0"><strong>n° {{$order->id}}</strong></p>
                                        </div>
                                        <div class="single-order-info">
                                            <h4 class="title-1 text-uppercase text-light-black mb-0">Date</h4>
                                            <p class="text-uppercase text-light-black mb-0"><strong>{{ $order->created_at->format('d/m/Y') }}</strong></p>
                                        </div>
                                        <div class="single-order-info">
                                            <h4 class="title-1 text-uppercase text-light-black mb-0">Total</h4>
                                            <p class="text-uppercase text-light-black mb-0"><strong>{{ $total }}</strong></p>
                                        </div>
                                        <div class="single-order-info">
                                            <h4 class="title-1 text-uppercase text-light-black mb-0">payment method</h4>
                                            <p class="text-uppercase text-light-black mb-0"><a href="#"><strong>check payment</strong></a></p>
                                        </div>
                                    </div>
                                    <div class="shop-cart-table check-out-wrap">
                                        <div class="row">
                                            <div class="col-md-6">
                                                @component('eCommerce.includes.resume', ['total' => $total])@endcomponent
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
