@extends('layout')

@section('content')
    <div class="shopping-cart-area  pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping-cart">
                        <div class="tab-content">
                            <div class="tab-pane active" id="order-complete">
                                <form action="#">
                                    <div class="thank-recieve bg-white mb-30">
                                        <p>Thank you. Your order has been received.</p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
