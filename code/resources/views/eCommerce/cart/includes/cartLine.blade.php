<tr>
    <td class="product-thumbnail text-left">
        <div class="single-product">
            <div class="product-img">
                <a href="{{ $line->product->url() }}"><img src="img/product/2.jpg"/></a>
            </div>
            <div class="product-info">
                <h4 class="post-title"><a class="text-light-black" href="{{ $line->product->url() }}">{{$line->product->name}}</a></h4>
            </div>
        </div>
    </td>
    <td class="product-price">{{ formatPrice($line->product->price) }}</td>
    <td class="product-quantity">
        <div class="cart-plus-minus">
            <input type="text" value="{{ $line->quantity }}" name="qtybutton" class="cart-plus-minus-box">
        </div>
    </td>
    <td class="product-subtotal">{{ formatPrice($line->totalLine) }}</td>
    <td class="product-remove">
        <a href="#"><i class="zmdi zmdi-close"></i></a>
    </td>
</tr>
