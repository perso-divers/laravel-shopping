@extends('layout')

@section('content')
    <div class="shopping-cart-area  pt-80 pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping-cart">
                        @if($cart->lines->count())
                            <!-- Nav tabs -->
                            <ul class="cart-page-menu nav row clearfix mb-30">
                                <li><a class="active" href="#shopping-cart" data-bs-toggle="tab">shopping cart</a></li>
                                <li><a href="#check-out" data-bs-toggle="tab">check out</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- shopping-cart start -->
                                <div class="tab-pane active" id="shopping-cart">
                                    <form action="#">
                                        <div class="shop-cart-table">
                                            <div class="table-content table-responsive">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th class="product-thumbnail">Product</th>
                                                        <th class="product-price">Price</th>
                                                        <th class="product-quantity">Quantity</th>
                                                        <th class="product-subtotal">Total</th>
                                                        <th class="product-remove">Remove</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($cart->lines as $line)
                                                            @component('eCommerce.cart.includes.cartLine', ['line' => $line])@endcomponent
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                @component('eCommerce.includes.resume', ['total' => $total])@endcomponent
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- shopping-cart end -->
                                <!-- check-out start -->
                                <div class="tab-pane" id="check-out">
                                    <div class="shop-cart-table check-out-wrap">
                                        <div class="row">
                                            <form class="row" action="{{ route('cart.saveAddresses') }}" method="POST">
                                                @csrf
                                                <div class="col-md-6">
                                                    <div class="billing-details pr-20">
                                                        @component('eCommerce.includes.address', [
                                                           'title' => 'Shipping address',
                                                           'type' => 'shipping',
                                                           'entity' => $cart->shipping_address
                                                       ])@endcomponent
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mt-xs-30">
                                                    <div class="billing-details pl-20">
                                                        @component('eCommerce.includes.address', [
                                                           'title' => 'Delivery address',
                                                           'type' => 'delivery',
                                                           'entity' => $cart->delivery_address
                                                       ])@endcomponent
                                                    </div>
                                                </div>
                                                <div>
                                                    <button type="submit" style="float: right" class="button-one submit-button mt-15">Save addresses</button>
                                                </div>
                                            </form>
                                            <div class="col-md-6">
                                                @component('eCommerce.includes.resume', ['total' => $total])@endcomponent
                                            </div>
                                            <!-- payment-method -->
                                            <div class="col-md-6">
                                                <div class="payment-method mt-60  pl-20">
                                                    <h4 class="title-1 title-border text-uppercase mb-30">payment method</h4>
                                                    <div class="payment-accordion">
                                                        @foreach($paymentProviders as $paymentProvider)
                                                            <h3 class="payment-accordion-toggle active">{{ $paymentProvider->name }}</h3>
                                                            <div class="payment-content default">
                                                                <p>{{ $paymentProvider->description }}</p>
                                                                <a href="{{ route('cart.validate', Str::of($paymentProvider->name)->slug ) }}">
                                                                    <img src="{{ $paymentProvider->icon }}"/>
                                                                </a>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="thank-recieve bg-white mb-30">
                                <p>Your cart is empty</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
