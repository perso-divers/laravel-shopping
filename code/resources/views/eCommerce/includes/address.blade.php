<h4 class="title-1 title-border text-uppercase mb-30">{{ $title }}</h4>
<input type="text" value="{{$entity?->firstname}}" name="{{$type}}[firstname]" placeholder="Your firstname here...">
<input type="text" value="{{$entity?->lastname}}" name="{{$type}}[lastname]" placeholder="Your lastname here...">
<input type="text" value="{{$entity?->street}}" name="{{$type}}[street]" placeholder="Your street here...">
<input type="text" value="{{$entity?->zipcode}}" name="{{$type}}[zipcode]" placeholder="Your zipcode here...">
<input type="text" value="{{$entity?->city}}" name="{{$type}}[city]" placeholder="Your city here...">
<input type="text" value="{{$entity?->country}}" name="{{$type}}[country]" placeholder="Your country here...">
