<div class="customer-login payment-details mt-30">
    <h4 class="title-1 title-border text-uppercase">payment details</h4>
    <table>
        <tbody>
        <tr>
            <td class="text-left">Cart total products</td>
            <td class="text-end">{{ formatPrice($total) }}</td>
        </tr>
        <tr>
            <td class="text-left">Order Total</td>
            <td class="text-end">{{ formatPrice($total) }}</td>
        </tr>
        </tbody>
    </table>
</div>
