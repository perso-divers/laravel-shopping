@extends('layout')

@section('content')
    <div class="purchase-online-area pt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center">
                        <h2 class="title-border">Purchase Online on Hurst</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <!-- Nav tabs -->
                    <ul class="tab-menu nav clearfix">
                        @foreach($categories as $category)
                            <li><a @if($loop->first) class="active" @endif href="#{{ Str::of($category->name)->slug() }}"
                                   data-bs-toggle="tab">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        @foreach($categories as $category)
                            <div class="tab-pane @if($loop->first) active @endif" id="{{ Str::of($category->name)->slug() }}">
                                <div class="row">
                                    @foreach($category->products as $product)
                                        @component('product.product.includes.item', ['product' => $product])@endcomponent
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($categories as $category)
        @foreach($category->products as $product)
            @component('product.product.includes.modal', ['product' => $product])@endcomponent
        @endforeach
    @endforeach
@endsection
