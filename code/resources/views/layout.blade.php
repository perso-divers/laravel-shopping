<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home one || Hurst</title>
    <meta name="description" content="Hurst – Furniture Store eCommerce HTML Template is a clean and elegant design – suitable for selling flower, cookery, accessories, fashion, high fashion, accessories, digital, kids, watches, jewelries, shoes, kids, furniture, sports….. It has a fully responsive width adjusts automatically to any screen size or resolution.">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>

    <!-- all css here -->
    <!-- bootstrap v5 css -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="/css/animate.min.css">
    <!-- jquery-ui.min css -->
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="/css/meanmenu.min.css">
    <!-- nivo-slider css -->
    <link rel="stylesheet" href="/lib/css/nivo-slider.css">
    <link rel="stylesheet" href="/lib/css/preview.css">
    <!-- slick css -->
    <link rel="stylesheet" href="/css/slick.min.css">
    <!-- lightbox css -->
    <link rel="stylesheet" href="/css/lightbox.min.css">
    <!-- material-design-iconic-font css -->
    <link rel="stylesheet" href="/css/material-design-iconic-font.css">
    <!-- All common css of theme -->
    <link rel="stylesheet" href="/css/default.css">
    <!-- style css -->
    <link rel="stylesheet" href="/style.css">
    <!-- shortcode css -->
    <link rel="stylesheet" href="/css/shortcode.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="/css/responsive.css">
    <!-- modernizr css -->
    <script src="/js/vendor/modernizr-3.11.2.min.js"></script>
</head>
<body>
<!-- WRAPPER START -->
<div class="wrapper">

    <!-- Mobile-header-top Start -->
    <div class="mobile-header-top d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- header-search-mobile start -->
                    <div class="header-search-mobile">
                        <div class="table">
                            <div class="table-cell">
                                <ul>
                                    <li><a class="search-open" href="#"><i class="zmdi zmdi-search"></i></a></li>
                                    <li><a href="login.html"><i class="zmdi zmdi-lock"></i></a></li>
                                    <li><a href="my-account.html"><i class="zmdi zmdi-account"></i></a></li>
                                    <li><a href="wishlist.html"><i class="zmdi zmdi-favorite"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- header-search-mobile start -->
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile-header-top End -->
    <!-- HEADER-AREA START -->
    <header id="sticky-menu" class="header">
        <div class="header-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 offset-md-4 col-7">
                        <div class="logo text-md-center">
                            <a href="{{ route('home') }}"><img src="/img/logo/logo.png"/></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-5">
                        <div class="mini-cart text-end">
                            <ul>
                                <li>
                                    @if( auth()->user() )
                                        <a class="cart-icon" href="{{route('cart.show')}}">
                                            <i class="zmdi zmdi-shopping-cart"></i>
                                            @if( $currentCart->count() )
                                                <span>{{ $currentCart->count() }}</span>
                                            @endif
                                        </a>
                                    @else
                                        <a class="cart-icon" href="{{route('auth.login')}}">
                                            <i class="zmdi zmdi-account"></i>
                                        </a>
                                    @endif
                                    @if($currentCart->count())
                                        <div class="mini-cart-brief text-left">
                                            <div class="cart-items">
                                                <p class="mb-0">You have <span>{{ $currentCart->count() }} items</span> in your shopping bag</p>
                                            </div>
                                                <div class="all-cart-product clearfix">
                                                    @foreach($cartLines as $cartLine)
                                                        <div class="single-cart clearfix">
                                                            <div class="cart-photo">
                                                                <a href="{{ $cartLine->product->url() }}"><img src="/img/cart/1.jpg"/></a>
                                                            </div>
                                                            <div class="cart-info">
                                                                <h5><a href="{{ $cartLine->product->url() }}">{{ $cartLine->product->name }}</a></h5>
                                                                <p class="mb-0">Price : {{ formatPrice($cartLine->product->price) }}</p>
                                                                <p class="mb-0">Qty : {{ $cartLine->quantity }} </p>
                                                                <span class="cart-delete"><a href="#"><i class="zmdi zmdi-close"></i></a></span>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            <div class="cart-totals">
                                                <h5 class="mb-0">Total {{ formatPrice($cartTotal) }}</h5>
                                            </div>
                                        </div>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MAIN-MENU START -->
        <div class="menu-toggle hamburger hamburger--emphatic d-none d-md-block">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
        <div class="main-menu  d-none d-md-block">
            <nav>
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    @if( auth()->user() )
                        <li><a href="{{ route('auth.logout') }}">Logout</a></li>
                    @endif
                </ul>
            </nav>
        </div>
        <!-- MAIN-MENU END -->
    </header>
    <!-- HEADER-AREA END -->
    <!-- Mobile-menu start -->
    <div class="mobile-menu-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 d-block d-md-none">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li><a href="{{ route('home') }}">Home</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile-menu end -->
    <!-- sidebar-search Start -->
    <div class="sidebar-search animated slideOutUp">
        <div class="table">
            <div class="table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 offset-md-2 p-0">
                            <div class="search-form-wrap">
                                <button class="close-search"><i class="zmdi zmdi-close"></i></button>
                                <form action="#">
                                    <input type="text" placeholder="Search here..." />
                                    <button class="search-button" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
    <!-- FOOTER START -->
    <footer>
        <!-- Footer-area start -->
        <div class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-footer">
                            <h3 class="footer-title  title-border">Contact Us</h3>
                            <ul class="footer-contact">
                                <li><span>Phone :</span>+33 6 25 49 29 00</li>
                                <li><span>Email :</span>agueneau@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="single-footer">
                            <h3 class="footer-title  title-border">accounts</h3>
                            <ul class="footer-menu">
                                <li><a href="#"><i class="zmdi zmdi-dot-circle"></i>My Account</a></li>
                                <li><a href="#"><i class="zmdi zmdi-dot-circle"></i>My Wishlist</a></li>
                                <li><a href="{{ route('cart.show') }}"><i class="zmdi zmdi-dot-circle"></i>My Cart</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="single-footer">
                            <h3 class="footer-title  title-border">shipping</h3>
                            <ul class="footer-menu">
                                @foreach($categories as $category)
                                    <li><a href="{{ $category->url() }}"><i class="zmdi zmdi-dot-circle"></i>{{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer-area end -->
        <!-- Copyright-area start -->
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="copyright">
                            <p class="mb-0">&copy; <a href=" https://themeforest.net/user/codecarnival/portfolio " target="_blank"> Antoine GUENEAU  </a> {{ date('Y') }}. All Rights Reserved.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="payment  text-md-end">
                            <a href="#"><img src="img/payment/1.png" alt="" /></a>
                            <a href="#"><img src="img/payment/3.png" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright-area start -->
    </footer>
    <!-- FOOTER END -->

</div>
<!-- WRAPPER END -->

<!-- all js here -->
<!-- jquery latest version -->
<script src="/js/vendor/jquery-3.6.0.min.js"></script>
<script src="/js/vendor/jquery-migrate-3.3.2.min.js"></script>
<!-- bootstrap js -->
<script src="/js/bootstrap.bundle.min.js"></script>
<!-- jquery.meanmenu js -->
<script src="/js/jquery.meanmenu.js"></script>
<!-- slick.min js -->
<script src="/js/slick.min.js"></script>
<!-- jquery.treeview js -->
<script src="/js/jquery.treeview.js"></script>
<!-- lightbox.min js -->
<script src="/js/lightbox.min.js"></script>
<!-- jquery-ui js -->
<script src="/js/jquery-ui.min.js"></script>
<!-- jquery.nivo.slider js -->
<script src="/lib/js/jquery.nivo.slider.js"></script>
<script src="/lib/home.js"></script>
<!-- jquery.nicescroll.min js -->
<script src="/js/jquery.nicescroll.min.js"></script>
<!-- countdon.min js -->
<script src="/js/countdon.min.js"></script>
<!-- wow js -->
<script src="/js/wow.min.js"></script>
<!-- plugins js -->
<script src="/js/plugins.js"></script>
<!-- main js -->
<script src="/js/main.js"></script>
</body>
</html>
